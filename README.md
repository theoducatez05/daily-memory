# Daily Memory

## Table of Contents

* [About the Game](#about-the-game)
  * [Functionnalities](#functionnalities)
  * [Built With](#built-with)
* [State of the game and version](#version-of-the-game)
  * [Actuality](#actuality)
  * [Version](#version)
* [Usage](#usage)  
  * [Screenshots](#screenshots)
* [Creators](#creators)
* [Contact](#contact)



## About The Game
<div align="center">
  <img src="Gitlab_images/dailymemory.jpg" width="300" height="200"/>
</div>
Daily Memory is an android and IOS application allowing anybody to learn to train efficiently his memory daily with the help of many memorisation techniques.

The  main goal of Daily Memory is to popularize memorization techniques to the public. Only few of them are known by most people but there is many more than that. They can be used by anybody to memorize efficiently and are easy to learn. 

Daily memory allows you to learn these techniques and practice efficiently right after that in a rewarding way. It is customizable and easy to use with a very simple interface. The statistic part also allows users to keep track of their progress and earn new badges to show off ! 

The idea and techniques behind our application come directly from the book "Tout sur la mémorire" by Tony Buzan.

## Functionnalities

<div align="center">
<img src="Gitlab_images/modules.JPG" width="450" height="250" />
</div>

* Game module

In this module users can use random lists to play with (random words or random grocery list). User also can save his own lists of 7 words to train with (Strings). During the game phase, the user has infinite time to memorize the words but his score decrease the more he waits to proceed. Right after that he has to write the words he memorized inside a new empty list. The final phase is the recapitulation of the score, the time and the correct words he got. The score is directly added to memory and is accessible in the stats module. 

* Learn module

In this module, the user can choose from a list of techniques he wants to learn. Some texts and information about them is given with examples to learn efficiently the technique inside each of them. At the end of each of them he can then directly access to the specific game module section to play right after learning the techniques.

* Statistics module

In this module the user can find charts of his previous scores (line and bar charts) grouped by day (average and sum). He can also have a look to the badges he earns with their description. 

* Parameter module

In this module the user can manage the notification process (enable them and set up the hour of the day) and can reinitiate his score.

### Built With

* [Unity](https://unity.com/)
* [Tout sur la mémoire - Tony Uzan](https://www.amazon.com/tout-sur-la-memoire/dp/2212552300)



## Version of the game

### Actuality

The first version of the game was finished in January 2021.

### Version

* 1.0 : January 2021
    * The Main menu
    * The Game section (List and gorcery list memorization)
    * The Learning section (Nombre-forme, Nombre-rime, Palais Mental)
    * The Stats section (Chart and Barchart visualisation, Badges)
    * The Parameters section (Dark mode, Notification Handler, statistics reinitialisation)

## Usage

### Screenshots

That is how the main menu looks like :
<div align="center">
<img src="Gitlab_images/main.jpg" width="150" height="300" />
</div>

There, you have to memorize the words:
<div align="center">
<img src="Gitlab_images/game1.jpg" width="150" height="300" />
</div>

This is where you have to write the memorized words:
<div align="center">
<img src="Gitlab_images/game2.jpg" width="150" height="300" />
</div>

This is where you can save you own lists:
<div align="center">
<img src="Gitlab_images/game3.jpg" width="150" height="300" />
</div>

The stats main page:
<div align="center">
<img src="Gitlab_images/stats1.jpg" width="150" height="300" />
</div>

The bar chart where scores are displayed:
<div align="center">
<img src="Gitlab_images/stats.jpg" width="150" height="300" />
</div>

The badges section (Owned and not owned) :
<div align="center">
<img src="Gitlab_images/badges.jpg" width="150" height="300" />
</div>

The notification tweek module:
<div align="center">
<img src="Gitlab_images/notification.jpg" width="150" height="300" />
</div>

## Creator

Of the 1.0 version :

* Théo Ducatez (Computer science engineering student)
* Dorian Zielinsky (Computer science engineering student)
* Arad Gorreshi (Computer science engineering student)
* Julien Revaud (Computer science engineering student)

** The logo comes from flaticon and has been made by Freepiks

## Contact

Théo Ducatez - [theo-ducatez](https://www.linkedin.com/in/theo-ducatez/) - theoducatez05@gmail.com

Project Link: [https://gitlab.com/theoducatez05/daily-memory](https://gitlab.com/theoducatez05/wasted)
