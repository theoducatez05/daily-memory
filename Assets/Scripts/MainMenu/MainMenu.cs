﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnPlay()
    {
        SceneManager.LoadScene("GameMenuScene");
    }

    public void ClickOnLearn()
    {
        SceneManager.LoadScene("Apprentissage");
    }

    public void ClickOnStatistics()
    {
        SceneManager.LoadScene("StatScene");
    }

    public void ClickOnParameters()
    {
        SceneManager.LoadScene("ParamMenu");
    }
}
