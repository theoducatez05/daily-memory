﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GraphPanel : MonoBehaviour
{
    [Header("Parameters")]

    /// <summary>
    /// The default max number of visible points
    /// </summary>
    [SerializeField]
    private int defaultMaxVisibilePointAmount = 14;

    [Space(10)]
    [Header("Sprites")]

    /// <summary>
    /// The sprite to use for dots
    /// </summary>
    [SerializeField]
    private Sprite pointSprite = default;

    /// <summary>
    /// The sprite to use for connection
    /// </summary>
    [SerializeField]
    private Sprite connectionSprite = default;

    /// <summary>
    /// The sprite to use for bars
    /// </summary>
    [SerializeField]
    private Sprite barSprite = default;

    [Space(10)]
    [Header("Labels")]
    /// <summary>
    /// The X axis prefab
    /// </summary>
    [SerializeField]
    private RectTransform labelXPrefab = default;

    /// <summary>
    /// The Y axis prefab
    /// </summary>
    [SerializeField]
    private RectTransform labelYPrefab = default;

    [Space(10)]
    [Header("Separators")]
    /// <summary>
    /// The separator for the X axis 
    /// </summary>
    [SerializeField]
    private RectTransform separatorX = default;

    /// <summary>
    /// The separator for the Y axis 
    /// </summary>
    [SerializeField]
    private RectTransform separatorY = default;

    /// <summary>
    /// The graphContainer
    /// </summary>
    private RectTransform graphContainer;

    [Space(10)]
    [Header("Tooltip")]
    [SerializeField]
    private RectTransform tooltip = default;

    [Space(10)]
    [Header("Objects container")]
    /// <summary>
    /// The list of all instantiated 
    /// </summary>
    [SerializeField]
    private RectTransform GraphObjects = default;

    [Space(10)]
    [Header("Buttons")]
    [SerializeField]
    /// <summary>
    /// The button to change visual
    /// </summary>
    private Button visualChangeButton = default;

    [Space(10)]
    [Header("Labels")]
    [SerializeField]
    private TextMeshProUGUI noDataLabel = default;

    /// <summary>
    /// The stats manager
    /// </summary>
    private StatsManager statsManager;

    /// <summary>
    /// The slider to move the graph
    /// </summary>
    private Slider slider;

    private TMP_Dropdown dropdownAgregation;

    private IGraphVisual currentGraphVisual;

    //Cached values
    private List<int> valuesListCached;
    private int maxVisiblePointAmountCached;
    private int lastValueIndexCached;

    private void Awake()
    {
        this.graphContainer = transform.Find("GraphContainer").GetComponent<RectTransform>();
        this.statsManager = StatsManager.GetStatsManager();

        //List<int> valueList = new List<int>() { 20, 190, 112,50,20, 54, 110, 90, 60, 44, 34, 30, 26, 34, 40,70,80,90,60,54,110, 90, 60, 44, 34, 30 , 26 , 34, 50 , 74, 80 , 72, 66, 54, 110, 90, 60, 44, 34, 30, 26, 34 };
        valuesListCached = this.statsManager.GetScoresForStatsMenu();

        //Set up the chart visual
        this.currentGraphVisual = new LineChartVisual(this.GraphObjects, pointSprite, connectionSprite);

        //Set up the slider
        this.slider = transform.GetComponentInChildren<Slider>();
        this.slider.minValue = this.defaultMaxVisibilePointAmount;
        if (this.defaultMaxVisibilePointAmount > valuesListCached.Count)
        {
            this.slider.maxValue = 0;
            this.slider.minValue = 0;
            this.slider.gameObject.SetActive(false);
        }
        else
        {
            this.slider.minValue = this.defaultMaxVisibilePointAmount;
            this.slider.maxValue = valuesListCached.Count;
            this.slider.gameObject.SetActive(true);
        }
        this.slider.value = valuesListCached.Count;

        //Slider listeners
        this.slider.onValueChanged.AddListener(value => {
            this.CleanGraph();
            this.ShowGraph(valuesListCached, this.statsManager.GetDatesForStatsMenu(), currentGraphVisual, this.defaultMaxVisibilePointAmount, Mathf.RoundToInt(value));
            });

        //Dropdown
        this.dropdownAgregation = transform.GetComponentInChildren<TMP_Dropdown>();
        this.dropdownAgregation.onValueChanged.AddListener(value =>{
            this.CleanGraph();

            if (value == 1)
            {
                valuesListCached = statsManager.GetScoresForStatsMenuAvg();
            }
            else
            {
                valuesListCached = statsManager.GetScoresForStatsMenu();
            }

            this.ShowGraph(valuesListCached, this.statsManager.GetDatesForStatsMenu(), currentGraphVisual, this.defaultMaxVisibilePointAmount, Mathf.RoundToInt(slider.value));
        });

        //Button listeners
        this.visualChangeButton.onClick.AddListener(() => NextGraphVisual()); ;

        //Display graph
        this.ShowGraph(valuesListCached, this.statsManager.GetDatesForStatsMenu(),currentGraphVisual, this.defaultMaxVisibilePointAmount);
    }

    /// <summary>
    /// Show the graph
    /// </summary>
    /// <param name="valuesList"></param>
    /// <param name="graphVisual"></param>
    /// <param name="maxVisibilePointAmount"></param>
    /// <param name="lastValueIndex"></param>
    /// <param name="getXLabel"></param>
    /// <param name="getYLabel"></param>
    private void ShowGraph(List<int> valuesList, List<string> datesList,IGraphVisual graphVisual, int maxVisibilePointAmount, int lastValueIndex = 0, Func<int, string> getXLabel = null, Func<float, string> getYLabel = null)
    {
        if (valuesList.Count == 0)
        {
            this.noDataLabel.gameObject.SetActive(true);
            return;
        }
        else
        {
            this.noDataLabel.gameObject.SetActive(false);
        }

        this.valuesListCached = valuesList;
        this.lastValueIndexCached = lastValueIndex;

        if (getXLabel == null)
        {
            getXLabel = delegate (int _i) { return _i.ToString(); };
        }

        if (getYLabel == null)
        {
            getYLabel = delegate (float _f) { return Mathf.RoundToInt(_f).ToString(); };
        }

        if (lastValueIndex <= 0)
        {
            lastValueIndex = valuesList.Count;
        }

        if (lastValueIndex < maxVisibilePointAmount)
        {
            maxVisibilePointAmount = lastValueIndex;
        }
        
        if (maxVisibilePointAmount > valuesList.Count)
        {
            maxVisibilePointAmount = valuesList.Count;
        }
        else
        {
            maxVisibilePointAmount = this.defaultMaxVisibilePointAmount;
        }

        if (maxVisibilePointAmount <= 0)
        {
            maxVisibilePointAmount = valuesList.Count;
        }

        this.maxVisiblePointAmountCached = maxVisibilePointAmount;

        //Get max and min scores
        this.GetMaxAndMinScores(valuesList, maxVisibilePointAmount, lastValueIndex, out float maxScore, out float minScore);
        maxScore *= 1.1f;

        //Graph sizes
        float graphHeight = this.graphContainer.sizeDelta.y;
        float maxYValue = maxScore+ (10-(maxScore%10));
        float xSpacing = this.graphContainer.sizeDelta.x / (maxVisibilePointAmount +1);

        //Set up the chart visual
        //LineChartVisual chartVisual = new LineChartVisual(this.GraphObjects, pointSprite, connectionSprite);

        //Compute the placement of the point
        int xIndex = 0;
        for (int i = Mathf.Max(lastValueIndex - maxVisibilePointAmount, 0); i < lastValueIndex; i++)
        {
            float xPosition = xSpacing + xIndex * xSpacing;
            float yPosition = (valuesList[i]/maxYValue) * graphHeight;

            //Adding the visual
            String tooltipText = getYLabel(valuesList[i]);
            graphVisual.AddGraphVisual(new Vector2(xPosition, yPosition), xSpacing*0.7f, tooltipText, this);

            //Create the X label for the point
            RectTransform labelX = Instantiate(this.labelXPrefab);
            labelX.GetComponent<TextMeshProUGUI>().text = datesList[i];
            labelX.SetParent(this.GraphObjects, false);
            labelX.anchoredPosition = new Vector2(xPosition, -20f);
            //labelX.GetComponent<TextMeshProUGUI>().text = getXLabel(i);

            //Create the X separators 
            RectTransform separatorX = Instantiate(this.separatorX);
            separatorX.SetParent(this.GraphObjects, false);
            float marging = -0;
            separatorX.anchoredPosition = new Vector2(xPosition, marging);
            separatorX.sizeDelta = new Vector2(graphHeight*2+(-1f*marging),separatorX.sizeDelta.y);

            xIndex++;
        }

        //Create the Y labels
        int separatorCount = 10;
        for (int i = 0; i <= separatorCount; i++)
        {
            RectTransform labelY = Instantiate(this.labelYPrefab);
            labelY.SetParent(this.GraphObjects, false);
            float normalizedValue = i * 1f / separatorCount;
            labelY.anchoredPosition = new Vector2(-10f, normalizedValue * graphHeight);
            labelY.GetComponent<TextMeshProUGUI>().text = getYLabel(normalizedValue * maxYValue);

            //Create the Y separators
            RectTransform separatorY = Instantiate(this.separatorY);
            separatorY.SetParent(this.GraphObjects, false);
            float marging = 0;
            separatorY.anchoredPosition = new Vector2(marging, normalizedValue * graphHeight);
            separatorY.sizeDelta = new Vector2(graphHeight * 2 + (-1f * marging), separatorY.sizeDelta.y);
        }
    }

    /// <summary>
    /// Change to the next visual
    /// </summary>
    private void NextGraphVisual()
    {
        if (this.currentGraphVisual is LineChartVisual)
        {
            this.currentGraphVisual = new BarChartVisual(this.GraphObjects, this.barSprite);
            foreach (RectTransform image in this.visualChangeButton.GetComponent<RectTransform>())
            {
                if (image.gameObject.name.Equals("LineButton"))
                    image.gameObject.SetActive(true);
                else if (image.gameObject.name.Equals("BarButton"))
                    image.gameObject.SetActive(false);
            }
        }
        else
        {
            this.currentGraphVisual = new LineChartVisual(this.GraphObjects, pointSprite, connectionSprite);
            foreach (RectTransform image in this.visualChangeButton.GetComponent<RectTransform>())
            {
                if (image.gameObject.name.Equals("BarButton"))
                    image.gameObject.SetActive(true);
                else if (image.gameObject.name.Equals("LineButton"))
                    image.gameObject.SetActive(false);
            }
        }

        this.CleanGraph();
        this.ShowGraph(this.valuesListCached, this.statsManager.GetDatesForStatsMenu(), this.currentGraphVisual, this.maxVisiblePointAmountCached, this.lastValueIndexCached);
    }

    /// <summary>
    /// Shows the tooltip and places it
    /// </summary>
    /// <param name="tooltipText">The text to put in the tooltip</param>
    /// <param name="anchoredPosition">The position</param>
    public void ShowTooltip(String tooltipText, Vector2 anchoredPosition)
    {
        this.tooltip.gameObject.SetActive(true);

        //Text
        TextMeshProUGUI tooltipTextUI = this.tooltip.gameObject.transform.GetComponentInChildren<TextMeshProUGUI>();
        tooltipTextUI.text = tooltipText;

        //Sizing
        float textPadding = 4f;
        Vector2 backgroundSize = new Vector2(
            tooltipTextUI.preferredWidth + textPadding * 2f,
            tooltipTextUI.preferredHeight + textPadding * 2f
        );

        tooltip.gameObject.transform.Find("Background").GetComponent<RectTransform>().sizeDelta = backgroundSize;

        //Position
        tooltip.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
        tooltip.gameObject.transform.SetAsLastSibling();

    }

    /// <summary>
    /// Hides the tooltip
    /// </summary>
    public void HideTooltip()
    {
        this.tooltip.gameObject.SetActive(false);
    }

    /// <summary>
    /// Clean the graph objects
    /// </summary>
    private void CleanGraph()
    {
        foreach (RectTransform child in this.GraphObjects.GetComponent<RectTransform>())
        {
            Destroy(child.gameObject);
        }
        currentGraphVisual.Refresh();
    }

    /// <summary>
    /// Method that gets the max score in the list given the maxvisible point amount
    /// </summary>
    /// <param name="list">The score list</param>
    /// <returns>The max score as an int</returns>
    private void GetMaxAndMinScores(List<int> list,int maxVisibilePointAmount, int lastValueIndex, out float _max, out float _min)
    {
        if (lastValueIndex <= 0)
        {
            lastValueIndex = list.Count;
        }

        if (list.Count <= 0)
        {
            _max = 0;
            _min = 0;
            return;
        }

        int max = list[0];
        int min = list[0];
        for (int i = Mathf.Max(lastValueIndex - maxVisibilePointAmount, 0); i < lastValueIndex; i++)
        {
            int value = list[i];
            if (value > max)
                max = value;

            if (value < min)
                min = value;
        }

        _max = max;
        _min = min;
    }
}
