﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StatMenu : MonoBehaviour
{

    [SerializeField]
    private GameObject currentScreen = default;

    [SerializeField]
    private GameObject listScreen = default;

    [SerializeField]
    private GameObject associationScreen = default;

    [SerializeField]
    private GameObject badgeScreen = default;

    [SerializeField]
    private GameObject mainScreen = default;

    [SerializeField]
    private RectTransform scoreText = default;

    // Start is called before the first frame update
    void Awake()
    {
        this.scoreText.GetComponent<TextMeshProUGUI>().text = StatsManager.GetStatsManager().GetTotalScore().ToString();
    }
 
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void LoadMainScreen()
    {
        this.currentScreen.gameObject.SetActive(false);
        this.currentScreen = this.mainScreen;
        this.currentScreen.SetActive(true);
    }

    public void LoadListScreen()
    {

        this.currentScreen.gameObject.SetActive(false);
        this.currentScreen = this.listScreen;
        this.currentScreen.SetActive(true);
    }

    public void LoadAssociationScreen()
    {
        this.currentScreen.gameObject.SetActive(false);
        this.currentScreen = this.associationScreen;
        this.currentScreen.SetActive(true);
    }

    public void LoadBadgeScreen()
    {
        this.currentScreen.gameObject.SetActive(false);
        this.currentScreen = this.badgeScreen;
        this.currentScreen.SetActive(true);
    }
}
