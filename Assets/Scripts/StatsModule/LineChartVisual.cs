﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LineChartVisual : IGraphVisual
{

    private RectTransform graphObjects;
    private Sprite pointSprite;
    private Sprite connectionSprite;
    private GameObject previousPoint;

    public LineChartVisual(RectTransform pGraphObjects, Sprite pPointSprite, Sprite pConnectionSprite)
    {
        this.graphObjects = pGraphObjects;
        this.pointSprite = pPointSprite;
        this.connectionSprite = pConnectionSprite;
        this.previousPoint = null;
    }

    public void AddGraphVisual(Vector2 graphPosition, float graphPositionWidth, string tooltipText, GraphPanel graph)
    {
        GameObject point = this.CreatePoint(graphPosition);

        //Events
        point.gameObject.AddComponent<Button>();
        point.GetComponent<Button>().targetGraphic = null;
        //barObject.GetComponent<Button>().onClick.AddListener(() => graph.ShowTooltip(tooltipText, new Vector2(graphPosition.x,graphPosition.y)));

        EventTrigger triggerDown = point.gameObject.AddComponent<EventTrigger>();

        var pointerDown = new EventTrigger.Entry();
        pointerDown.eventID = EventTriggerType.PointerDown;
        pointerDown.callback.AddListener((e) => graph.ShowTooltip(tooltipText, new Vector2(graphPosition.x-120, graphPosition.y)));

        EventTrigger triggerUp = point.gameObject.AddComponent<EventTrigger>();

        var pointerUp = new EventTrigger.Entry();
        pointerUp.eventID = EventTriggerType.PointerUp;
        pointerUp.callback.AddListener((e) => graph.HideTooltip());

        triggerDown.triggers.Add(pointerDown);
        triggerUp.triggers.Add(pointerUp);

        //Only do this for the points after the first one
        if (this.previousPoint != null)
            this.CreatePointConnection(previousPoint.GetComponent<RectTransform>().anchoredPosition, point.GetComponent<RectTransform>().anchoredPosition);

        this.previousPoint = point;
    }

    public void Refresh()
    {
        this.previousPoint = null;
    }

    /// <summary>
    /// Function that creates a dot on the graph
    /// </summary>
    /// <param name="anchoredPosition">The position on the graph</param>
    private GameObject CreatePoint(Vector2 anchoredPosition)
    {
        //Create the object
        GameObject point = new GameObject("Point", typeof(Image));

        //Init position and sprite
        point.transform.SetParent(this.graphObjects, false);
        point.gameObject.GetComponent<Image>().sprite = this.pointSprite;

        //Positioning
        RectTransform pointTransform = point.GetComponent<RectTransform>();
        pointTransform.anchoredPosition = anchoredPosition;
        pointTransform.sizeDelta = new Vector2(25, 25);
        pointTransform.anchorMin = new Vector2(0, 0);
        pointTransform.anchorMax = new Vector2(0, 0);

        return point;
    }

    /// <summary>
    /// Create a connexion with a sprite between point 1 and point 2
    /// </summary>
    /// <param name="point1Postion">The anchored position of the first point</param>
    /// <param name="point2Position">The anchored position of the second point</param>
    private void CreatePointConnection(Vector2 point1Postion, Vector2 point2Position)
    {
        //Create the object
        GameObject connection = new GameObject("Connection", typeof(Image));

        //Init position and sprite
        connection.transform.SetParent(this.graphObjects, false);
        connection.gameObject.GetComponent<Image>().sprite = this.connectionSprite;
        connection.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

        //Positionning
        RectTransform connectionTransform = connection.GetComponent<RectTransform>();
        Vector2 direction = (point2Position - point1Postion).normalized;
        float distance = Vector2.Distance(point1Postion, point2Position);
        connectionTransform.anchorMin = new Vector2(0, 0);
        connectionTransform.anchorMax = new Vector2(0, 0);
        connectionTransform.sizeDelta = new Vector2(distance, 10f);
        connectionTransform.anchoredPosition = point1Postion + direction * distance * 0.5f;
        connectionTransform.localEulerAngles = new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
    }
}
