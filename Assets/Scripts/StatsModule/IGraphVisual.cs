﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface Allowing us to use different types of graphs (bar and line)
/// </summary>
public interface IGraphVisual
{
    /// <summary>
    /// Method to create a graph visual (a point or a bar) independantly from its type
    /// </summary>
    /// <param name="graphPosition">The position of the graph (container)</param>
    /// <param name="graphPositionWidth">The width for the bars of the bar chart</param>
    void AddGraphVisual(Vector2 graphPosition, float graphPositionWidth, string tooltipText, GraphPanel graph);

    /// <summary>
    /// Called before redrawing the chart (for propertises initialisation)
    /// </summary>
    void Refresh();
}
