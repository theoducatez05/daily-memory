﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BarChartVisual : IGraphVisual
{

    private RectTransform graphObjects;
    private Sprite barSprite;

    public BarChartVisual(RectTransform pGraphObjects, Sprite pBarSprite)
    {
        this.graphObjects = pGraphObjects;
        this.barSprite = pBarSprite;
    }

    public void AddGraphVisual(Vector2 graphPosition, float graphPositionWidth, string tooltipText, GraphPanel graph)
    {
        GameObject barObject = this.CreateBar(graphPosition,  graphPositionWidth);
        barObject.gameObject.AddComponent<Button>();
        barObject.GetComponent<Button>().targetGraphic = null;
        //barObject.GetComponent<Button>().onClick.AddListener(() => graph.ShowTooltip(tooltipText, new Vector2(graphPosition.x,graphPosition.y)));
        
        EventTrigger triggerDown = barObject.gameObject.AddComponent<EventTrigger>();
        
        var pointerDown = new EventTrigger.Entry();
        pointerDown.eventID = EventTriggerType.PointerDown;
        pointerDown.callback.AddListener((e) => graph.ShowTooltip(tooltipText, graphPosition));

        EventTrigger triggerUp = barObject.gameObject.AddComponent<EventTrigger>();

        var pointerUp = new EventTrigger.Entry();
        pointerUp.eventID = EventTriggerType.PointerUp;
        pointerUp.callback.AddListener((e) => graph.HideTooltip());

        triggerDown.triggers.Add(pointerDown);
        triggerUp.triggers.Add(pointerUp);
    }

    public void Refresh()
    {
        
    }

    /// <summary>
    /// Function that creates a bar on the graph
    /// </summary>
    /// <param name="anchoredPosition">The position on the graph</param>
    private GameObject CreateBar(Vector2 anchoredPosition, float barWidth)
    {
        //Create the object
        GameObject point = new GameObject("Bar", typeof(Image));

        //Init position and sprite
        point.transform.SetParent(this.graphObjects, false);
        point.gameObject.GetComponent<Image>().sprite = this.barSprite;

        //Positioning
        RectTransform pointTransform = point.GetComponent<RectTransform>();
        pointTransform.anchoredPosition = new Vector2(anchoredPosition.x,0);
        pointTransform.sizeDelta = new Vector2(barWidth, anchoredPosition.y);
        pointTransform.anchorMin = new Vector2(0, 0);
        pointTransform.anchorMax = new Vector2(0, 0);
        pointTransform.pivot = new Vector2(0.5f, 0);

        return point;
    }
}

