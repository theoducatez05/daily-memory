﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    [SerializeField]
    private bool cleanStats = default;

    [SerializeField]
    private bool addFakeScores = default;

    /// <summary>
    /// The score string key
    /// </summary>
    private readonly string scorePref = "TotalScore";
    /// <summary>
    /// The stats string key
    /// </summary>
    private readonly string statsPref = "StatsPref";

    /// <summary>
    /// The badges string key
    /// </summary>
    private readonly string badgesPref = "BadgesPref";

    /// <summary>
    /// The total Score
    /// </summary>
    private int totalScore;


    /// <summary>
    /// The score list
    /// </summary>
    private List<int> scoresList;

    /// <summary>
    /// The dates list
    /// </summary>
    private List<string> datesList;

    private List<String> badgesList;

    /// <summary>
    /// Self
    /// </summary>
    private static StatsManager statsManager;

    // Start is called before the first frame update
    void Awake()
    {
        //Mono
        if (statsManager != null && statsManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            statsManager = this;
        }

        //Debug only
        if (this.cleanStats)
        {
            this.ClearStats();
        }

        //Verification for prefs
        if (!PlayerPrefs.HasKey(this.statsPref))
        {
            PlayerPrefs.SetString(this.statsPref,"");
        }

        if (!PlayerPrefs.HasKey(this.scorePref))
        {
            PlayerPrefs.SetInt(this.scorePref,0);
        }

        if (!PlayerPrefs.HasKey(this.badgesPref))
        {
            PlayerPrefs.SetString(this.badgesPref,"");
        }

        //Getting the stats pref
        this.totalScore = PlayerPrefs.GetInt(this.scorePref);

        this.datesList = new List<string>();
        this.scoresList = new List<int>();
        this.badgesList = new List<string>();
        string stats = PlayerPrefs.GetString(this.statsPref);

        if (!stats.Equals(""))
        {
            foreach (string line in stats.Split('\n'))
            {
                if (!line.Equals(""))
                {
                    string[] splittedLine = line.Split(',');
                    this.datesList.Add(splittedLine[0]);
                    this.scoresList.Add(Int32.Parse(splittedLine[1]));
                }
            }
        }

        string badges = PlayerPrefs.GetString(this.badgesPref);
        if (!badges.Equals(""))
        {
            foreach (string badge in badges.Split(','))
            {
                if (!badge.Equals(""))
                {
                    this.badgesList.Add(badge);
                }
            }
        }

        //Debug Only
        if (this.addFakeScores)
        {
            this.AddFakeScores();
        }

        this.CheckForBadges();

        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Add a new score to the score list and saves it to player prefs
    /// Also add to total score
    /// </summary>
    /// <param name="score">The score to add</param>
    public void AddAndSaveScore(int score)
    {
        //add date
        String date = DateTime.Now.ToString("d/M/yyyy");
        this.datesList.Add(date);

        //add score
        this.scoresList.Add(score);

        if (this.scoresList.Count != this.datesList.Count)
        {
            throw new Exception("Un probleme est survenu avec la sauvegarde des scores: Les listes ne sont pas de même taille");
        }

        //saving scores
        this.SaveScores();

        //Add to total score
        this.AddAndSaveTotalScore(score);
    }

    /// <summary>
    /// Saves the score
    /// </summary>
    public void SaveScores()
    {
        //Make sure that lists are the same size
        if (this.scoresList.Count != this.datesList.Count)
        {
            throw new Exception("Un probleme est survenu avec la sauvegarde des scores");
        }

        if (this.scoresList.Count <= 0)
        {
            PlayerPrefs.SetString(this.statsPref,"");
            return;
        }

        //Create the builder
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.datesList.Count; i++)
        {
            sb.Append(this.datesList[i]);
            sb.Append(",");
            sb.Append(this.scoresList[i]);
            sb.Append('\n');
        }

        PlayerPrefs.SetString(this.statsPref,sb.ToString());
    }

    /// <summary>
    /// Setter for the score. It also saves the total score to prefs
    /// </summary>
    /// <param name="add">The score to add</param>
    public void AddAndSaveTotalScore(int add)
    {
        this.totalScore += add;
        this.SaveTotalScore();
    }

    /// <summary>
    /// Saves the total score to player prefs
    /// </summary>
    private void SaveTotalScore()
    {
        PlayerPrefs.SetInt(this.scorePref,this.totalScore);
    }

    /// <summary>
    /// Getter for scores in a way that can be read by the stat menu
    /// </summary>
    /// <returns>The score list</returns>
    public List<int> GetScoresForStatsMenu()
    {
        if (this.scoresList.Count == 0)
        {
            return this.scoresList;
        }

        List<int> list = new List<int>();

        string previous = "None";
        int sum = 0;

        for (int i = 0; i < this.scoresList.Count; i++)
        {
            if (!previous.Equals("None"))
            {
                if(!previous.Equals(this.datesList[i]))
                {
                    list.Add(sum);
                    sum = 0;
                }
                sum += this.scoresList[i];
            }
            else
            {
                sum += this.scoresList[i];
            }
            previous = this.datesList[i];
        }

       
        list.Add(sum); 
        
        return list;
    }

    /// <summary>
    /// Get the score for the menu
    /// </summary>
    /// <returns>The score list for average</returns>
    public List<int> GetScoresForStatsMenuAvg()
    {
        if (this.scoresList.Count == 0)
        {
            return this.scoresList;
        }

        List<int> list = new List<int>();

        string previous = "None";
        int sum = 0;
        int count = 1;

        for (int i = 0; i < this.scoresList.Count; i++)
        {
            if (!previous.Equals("None"))
            {
                if (!previous.Equals(this.datesList[i]))
                {
                    list.Add(sum/count);
                    sum = 0;
                    count = 1;
                }
                else
                {
                    count += 1;
                }

                sum += this.scoresList[i];
            }
            else
            {
                sum += this.scoresList[i];
            }
            previous = this.datesList[i];
        }


        list.Add(sum/count);

        return list;
    }

    /// <summary>
    /// Getter for dates
    /// </summary>
    /// <returns>The date list without multiples ones</returns>
    public List<String> GetDatesForStatsMenu()
    {
        List<string> list = new List<string>();
        string previous = "None";
        
        foreach(string date in this.datesList)
        {
            if (previous.Equals("None"))
            {
                list.Add(date);
            }
            else
            {
                if (!date.Equals(previous))
                {
                    list.Add(date);
                }
            }

            previous = date;
        }
        
        return list;
    }

    /// <summary>
    /// Getter for the total score
    /// </summary>
    /// <returns>The total score</returns>
    public int GetTotalScore()
    {
        return this.totalScore;
    }

    /// <summary>
    /// Clear the stats prefs
    /// </summary>
    public void ClearStats()
    {
        this.datesList = new List<string>();
        this.scoresList = new List<int>();
        this.badgesList = new List<string>();
        this.totalScore = 0;

        PlayerPrefs.SetInt(this.scorePref, 0);
        PlayerPrefs.SetString(this.statsPref, "");
        PlayerPrefs.SetString(this.badgesPref,"");
    }

    /// <summary>
    /// Get the instance
    /// </summary>
    /// <returns></returns>
    public static StatsManager GetStatsManager()
    {
        return statsManager;
    }

    /// <summary>
    /// Saves the badges
    /// </summary>
    private void SaveBadges()
    {
        if (this.badgesList.Count <= 0)
        {
            PlayerPrefs.SetString(this.badgesPref, "");
            return;
        }

        //Create the builder
        bool first = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.badgesList.Count; i++)
        {
            if (!first)
            {
                sb.Append(',');
            }
            else
            {
                first = !first;
            }
            sb.Append(this.badgesList[i]);
        }

        PlayerPrefs.SetString(this.badgesPref, sb.ToString());
    }

    /// <summary>
    /// Check for badge update
    /// </summary>
    /// <returns></returns>
    public int CheckForBadges()
    {
        int numbers = 0;

        if (this.totalScore >= 100)
        {
            if (!this.badgesList.Contains("1")) {
                this.badgesList.Add("1");
                numbers++;
            }
        }
        if (this.totalScore >= 250)
        {
            if (!this.badgesList.Contains("2"))
            {
                this.badgesList.Add("2");
                numbers++;
            }
        }
        if (this.totalScore >= 500)
            {
            if (!this.badgesList.Contains("3"))
            {
                this.badgesList.Add("3");
                numbers++;
            }
        }
        if (this.totalScore >= 1000)
                {
            if (!this.badgesList.Contains("4"))
            {
                this.badgesList.Add("4");
                numbers++;
            }
        }
        if (this.totalScore >= 2500)
                    {
            if (!this.badgesList.Contains("5"))
            {
                this.badgesList.Add("5");
                numbers++;
            }
        }
        if (this.totalScore >= 5000)
                        {
            if (!this.badgesList.Contains("6"))
            {
                this.badgesList.Add("6");
                numbers++;
            }
        }
        if (this.totalScore >= 10000)
        {
            if (!this.badgesList.Contains("7"))
            {
                this.badgesList.Add("7");
                numbers++;
            }
        }

        this.SaveBadges();

        return numbers;
    }

    /// <summary>
    /// Get list
    /// </summary>
    /// <returns>The badge list</returns>
    public List<string> getBadgesList()
    {
        return this.badgesList;
    }

    /// <summary>
    /// Debug for fake score
    /// </summary>
    private void AddFakeScores()
    {
        int sum = 0;

        int score = 50;
        this.datesList.Add("10/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 45;
        this.datesList.Add("15/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 105;
        this.datesList.Add("20/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 75;
        this.datesList.Add("26/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 105;
        this.datesList.Add("27/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 110;
        this.datesList.Add("29/12/2020");
        this.scoresList.Add(score);
        sum += score;

        score = 100;
        this.datesList.Add("03/01/2021");
        this.scoresList.Add(score);
        sum += score;

        score = 100;
        this.datesList.Add("04/01/2021");
        this.scoresList.Add(score);
        sum += score;

        score = 25;
        this.datesList.Add("04/01/2021");
        this.scoresList.Add(score);
        sum += score;

        score = 10;
        this.datesList.Add("04/01/2021");
        this.scoresList.Add(score);
        sum += score;

        score = 75;
        this.datesList.Add("06/01/2021");
        this.scoresList.Add(score);
        sum += score;

        score = 100;
        this.datesList.Add("07/01/2021");
        this.scoresList.Add(score);
        sum += score;

        this.AddAndSaveTotalScore(sum);
        this.SaveScores();
    }
}
