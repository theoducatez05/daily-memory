﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIModeResolver : MonoBehaviour
{
    private const float Grey = 0.439f;
    //private readonly Color _grey = new Color(Grey, Grey, Grey, 1);
    private readonly Color _grey = new Color32(112, 112, 112, 255);
    private const float DarkGrey = 0.07f;
    private readonly Color _darkGrey = new Color(DarkGrey, DarkGrey, DarkGrey, 1);

    public void resolveMode()
    {
        /*foreach(var textMeshPro in GetComponentsInChildren<TextMeshProUGUI>(true))
        {
            textMeshPro.color = textMeshPro.color == _grey ? Color.white : _grey;
        }*/

        GameObject background = GameObject.Find("Background");
        Image backgroundImage = background.GetComponent<Image>();
        backgroundImage.color = backgroundImage.color == Color.white ? _darkGrey : Color.white;
    }
}
