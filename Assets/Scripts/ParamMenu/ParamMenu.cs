﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ParamMenu : MonoBehaviour
{

    public GameObject paramPage;
    public GameObject creditPage;
    public GameObject contactPage;
    public GameObject scorePopUp;
    public GameObject notificationpage;
    private UIModeResolver _modeResolver;

    private void Awake()
    {
        _modeResolver = FindObjectOfType<UIModeResolver>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnDarkTheme()
    {
        //Debug.Log("TODO : Change the theme of the app and the text into light theme");
        _modeResolver.resolveMode();
    }

    public void ClickOnManageNotif()
    {
        setInactiveAllPage();
        this.notificationpage.SetActive(true);
    }

    /// <summary>
    /// Set active the creditPage Game Object
    /// </summary>
    public void ClickOnCredits()
    {
        setInactiveAllPage();
        creditPage.SetActive(true);
    }

    public void ClickOnContact()
    {
        setInactiveAllPage();
        contactPage.SetActive(true);
    }

    public void ClickOnResetScore()
    {
        this.scorePopUp.SetActive(true);
    }

    public void ClickNoScorePopup()
    {
        this.scorePopUp.SetActive(false);
    }

    public void CallResetScore()
    {
        StatsManager.GetStatsManager().ClearStats();
        this.scorePopUp.SetActive(false);
    }

    /// List of function for return button

    /// <summary>
    /// Load the Main Menu Scene
    /// </summary>
    public void ClickOnReturnButtonToMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    /// <summary>
    /// Set active the paramPage game object
    /// </summary>
    public void ClickOnReturnButtonToParamMenu()
    {
        setInactiveAllPage();
        paramPage.SetActive(true);
    }

    /// Generic functions
    
    public void setInactiveAllPage()
    {
        paramPage.SetActive(false);
        creditPage.SetActive(false);
        contactPage.SetActive(false);
        notificationpage.SetActive(false);
    }
}
