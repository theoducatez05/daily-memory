﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotificationPage : MonoBehaviour
{
    private readonly string notifActivatePref = "NotifActivation";
    private readonly string notifTimePref = "NotifTime";

    private string timeNotif;
    private Boolean activateNotif;

    [SerializeField]
    private TextMeshProUGUI hoursLabel = default;
    [SerializeField]
    private TextMeshProUGUI minutesLabel = default;

    [SerializeField]
    private Button hoursUpButton = default;
    [SerializeField]
    private Button hoursDownButton = default;
    [SerializeField]
    private Button minutesUpButton = default;
    [SerializeField]
    private Button minutesDownButton = default;
    [SerializeField]
    private Button sendNotifButton = default;

    [SerializeField]
    private Toggle activationCheckBox = default;

    [SerializeField]
    private GameObject feedBackPopUp = default;

    [SerializeField]
    private bool clearNotif = default;

    // Start is called before the first frame update
    void Awake()
    {

        //Retreive time and set it up
        this.timeNotif = PlayerPrefs.GetString(this.notifTimePref);

        //Retrieve l'activation et la set
        bool activate = false;
        if (PlayerPrefs.GetInt(this.notifActivatePref) == 1)
        {
            activate = true;
        }
        this.activateNotif = activate;
    }

    private void Start()
    {
        if (this.clearNotif)
        {
            this.clearAll();
        }

        //put the good time
        if (!this.timeNotif.Equals(""))
        {
            string[] timeSplit = this.timeNotif.Split(':');
            this.hoursLabel.text = timeSplit[0];
            this.minutesLabel.text = timeSplit[1];
        }
        else
        {
            this.timeNotif = "18:30";
            this.SaveTime();
        }

        //put the good activation
        this.activationCheckBox.isOn = this.activateNotif ;


        this.hoursUpButton.onClick.AddListener(() => {
            this.AddHour();
        });

        this.hoursDownButton.onClick.AddListener(() => {
            this.RemoveHour();
        });

        this.minutesUpButton.onClick.AddListener(() => {
            this.AddMinute();
        });

        this.minutesDownButton.onClick.AddListener(() => {
            this.RemoveMinute();
        });

        this.sendNotifButton.onClick.AddListener(() => {
            MobileNotificationManager.GetMobileNotificationManager().SendNotifNow();
        });
    }

    public void SaveTime()
    {
        string timeSave = this.hoursLabel.text + ":" + this.minutesLabel.text;
        this.timeNotif = timeSave;
        PlayerPrefs.SetString(this.notifTimePref,timeSave);
        if (this.activateNotif)
        {
            Debug.Log("ON envoit la demande de creation notif");
            MobileNotificationManager.GetMobileNotificationManager().setNotifTime(this.timeNotif);
        }

        this.feedBackPopUp.SetActive(true);
    }

    public void ClosePopUp()
    {
        this.feedBackPopUp.SetActive(false);
    }

    public void SaveActivation()
    {
        this.activateNotif = this.activationCheckBox.isOn;
        int value = 0;
        if (this.activateNotif)
        {
            value = 1;
            if (!this.timeNotif.Equals(""))
            {
                MobileNotificationManager.GetMobileNotificationManager().setNotifTime(this.timeNotif);
            }
        }
        else
        {
            MobileNotificationManager.GetMobileNotificationManager().DisableNotifs();
        }
        
        PlayerPrefs.SetInt(this.notifActivatePref,value);
        Debug.Log("New: "+this.activateNotif+" , " + PlayerPrefs.GetInt(this.notifActivatePref));
    }

    public void clearAll()
    {
        this.timeNotif = "";
        this.activateNotif = false;

        PlayerPrefs.SetString(this.notifTimePref,"");
        PlayerPrefs.SetInt(this.notifActivatePref, 0);

        MobileNotificationManager.GetMobileNotificationManager().DisableNotifs();
    }

    private void AddHour()
    {
        int hour = Int32.Parse(this.hoursLabel.text);
        
        if (hour == 23)
        {
            this.hoursLabel.text = "00";
        }
        else
        {
            this.hoursLabel.text = (hour+1).ToString();
        }
    }

    private void RemoveHour()
    {
        int hour = Int32.Parse(this.hoursLabel.text);

        if (hour == 0)
        {
            this.hoursLabel.text = "23";
        }
        else
        {
            this.hoursLabel.text = (hour-1).ToString();
        }
    }

    private void AddMinute()
    {
        int minute = Int32.Parse(this.minutesLabel.text);

        if (minute == 59)
        {
            this.minutesLabel.text = "00";
        }
        else
        {
            this.minutesLabel.text = (minute+1).ToString();
        }
    }

    private void RemoveMinute()
    {
        int minute = Int32.Parse(this.minutesLabel.text);

        if (minute == 0)
        {
            this.minutesLabel.text = "59";
        }
        else
        {
            this.minutesLabel.text = (minute-1).ToString();
        }
    }
}
