﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadgesPanel : MonoBehaviour
{
    private List<string> badgesList;

    private void Awake()
    {
        //Recuperer la liste des badges recus
        this.badgesList = StatsManager.GetStatsManager().getBadgesList();

        foreach (Transform child in this.transform)
        {
            if (badgesList.Contains(child.gameObject.name))
            {
                child.transform.GetChild(child.transform.childCount - 1).gameObject.SetActive(false);
            }
        }
    }
}
