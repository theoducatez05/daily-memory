﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Notifications.Android;
using System;

public class MobileNotificationManager : MonoBehaviour
{

    public AndroidNotificationChannel defaultNotificationChannel;
    private readonly int identifier = 1;

    /// <summary>
    /// Self
    /// </summary>
    private static MobileNotificationManager notificationManager;

    // Start is called before the first frame update
    void Start()
    {
        //Mono
        if (notificationManager != null && notificationManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            notificationManager = this;
        }

        defaultNotificationChannel = new AndroidNotificationChannel()
        {
            Id = "default_channel",
            Name = "Default Channel",
            Description = "For Generic Notifications",
            Importance = Importance.High,
        };

        AndroidNotificationCenter.RegisterNotificationChannel(defaultNotificationChannel);

        DontDestroyOnLoad(this);
    }

    public void SendNotifNow()
    {
        AndroidNotification notification = new AndroidNotification()
        {
            Title = "Daily Memory",
            Text = "C'est l'heure d'entraîner son cerveau !",
            SmallIcon = "default",
            LargeIcon = "logo",
            FireTime = System.DateTime.Now,
        };

        AndroidNotificationCenter.SendNotification(notification, "default_channel");
    }

    public void setNotifTime(string time)
    {
        this.DisableNotifs();

        if (!time.Equals(""))
        {
            Debug.Log("Creation notification avec "+time);
            string[] splittedTime = time.Split(':');
            int hour = Int32.Parse(splittedTime[0]);
            int minute = Int32.Parse(splittedTime[1]);

            DateTime now = DateTime.Now;

            AndroidNotification notification = new AndroidNotification()
            {
                Title = "Daily Memory",
                Text = "C'est l'heure d'entraîner son cerveau !",
                SmallIcon = "default",
                LargeIcon = "logo",
                FireTime = new DateTime(now.Year, now.Month, now.Day, hour, minute, 0),//.AddDays(1),
                RepeatInterval = new TimeSpan(1, 0, 0, 0),
                ShouldAutoCancel = true
            };

            AndroidNotificationCenter.SendNotificationWithExplicitID(notification, "default_channel", this.identifier);
        }
    }

    public void DisableNotifs()
    {
        AndroidNotificationCenter.CancelAllScheduledNotifications();
    }

    public static MobileNotificationManager GetMobileNotificationManager(){
        return notificationManager;
    }
}
