﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ListTypeMenuAUTOManager : MonoBehaviour
{

    public GameMenuSceneScreenManager screenManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnRandomList()
    {
        PlayerPrefs.SetInt("listType", GameManager.RANDOM_LIST);
        SceneManager.LoadScene("ListGameScene");
        Debug.Log("TODO : Launch the game with Random words");
    }

    public void ClickOnCourseList()
    {
        PlayerPrefs.SetInt("listType", GameManager.SHOPPING_LIST);
        SceneManager.LoadScene("ListGameScene");
        Debug.Log("TODO : Launch the game with random words from COURSE LIST");
    }

    public void ClickOnBack()
    {
        screenManager.ShowMainGameMenuScreen();
    }
}
