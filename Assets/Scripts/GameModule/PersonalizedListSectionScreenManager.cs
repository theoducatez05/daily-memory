﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalizedListSectionScreenManager : MonoBehaviour
{
    public GameMenuSceneScreenManager mainScreenManager;
    
    public GameObject listTypeMenuScreenPERSO;

    public GameObject listModificationScreen;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        DeactivateAllScreens();
        ShowListTypeMenuScreenPERSO();
    }

    private void DeactivateAllScreens()
    {
        listTypeMenuScreenPERSO.SetActive(false);
        listModificationScreen.SetActive(false);
    }

    public void ShowListTypeMenuScreenPERSO()
    {
        DeactivateAllScreens();
        listTypeMenuScreenPERSO.SetActive(true);
    }

    public void ShowListModificationScreen(string keyName)
    {
        DeactivateAllScreens();
        listModificationScreen.GetComponent<ListModificationManager>().keyName = keyName;
        Debug.Log("MAIN SCREENMANAGER : " + listModificationScreen.GetComponent<ListModificationManager>().keyName);
        listModificationScreen.SetActive(true);
    }

    public void GoBackToMainMenu()
    {
        mainScreenManager.ShowMainGameMenuScreen();
    }
}
