﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuSceneScreenManager : MonoBehaviour
{

    public GameObject mainGameMenuScreen;

    public GameObject listTypeMenuScreen;

    public GameObject personalizedListSection;

    // Start is called before the first frame update
    void Start()
    {
        ShowMainGameMenuScreen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DeactivateAllScreens()
    {
        mainGameMenuScreen.SetActive(false);
        listTypeMenuScreen.SetActive(false);
        personalizedListSection.SetActive(false);
    }

    public void ShowMainGameMenuScreen()
    {
        DeactivateAllScreens();
        mainGameMenuScreen.SetActive(true);
    }

    public void ShowListTypeMenuScreen()
    {
        DeactivateAllScreens();
        listTypeMenuScreen.SetActive(true);
    }

    public void ShowPersonalizedListSection()
    {
        DeactivateAllScreens();
        personalizedListSection.SetActive(true);
    }
}
