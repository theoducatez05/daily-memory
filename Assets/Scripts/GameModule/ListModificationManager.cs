﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ListModificationManager : MonoBehaviour
{
    public PersonalizedListSectionScreenManager screenManager;

    public string keyName { get; set; } //the name of the key inside the playerPref where to find the list of words

    public GameObject listTitleField;
    public GameObject placeHolder;

    public WordsListManager wordsListManager;

    private string title;

    // Start is called before the first frame update
    void Awake()
    {
        title = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        if (keyName == null)
        {
            keyName = "None";
        }
        listTitleField.GetComponent<TMP_InputField>().text = "";
        placeHolder.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString(keyName + "Title", "Titre de la liste");
        if (PlayerPrefs.HasKey(keyName))
        {
            wordsListManager.ShowFillInWordsList(7, CreateListViaString(PlayerPrefs.GetString(keyName)));
            Debug.Log("LISTE DE MOTS : " + PlayerPrefs.GetString(keyName));
        }
        else
        {
            wordsListManager.ShowFillInWordsList(7);
        }
    }

    public void AfterTitleModification(string title)
    {
        this.title = title;
    }

    private List<string> CreateListViaString(string words)
    {
        List<string> wordsList = new List<string>(words.Split('\n'));
        wordsList.Remove("");
        return wordsList;
    }

    public void SaveModifications()
    {
        if (this.title != null && this.title != "")
        {
            PlayerPrefs.SetString(keyName + "Title", title);
            Debug.Log("SAVE keyname : " + keyName);
        }
        string stringWordsList = "";
        wordsListManager.GetSavedGivenWords().ForEach(w => {
            if (w != "")
            {
                stringWordsList += w + "\n";
            }
        }
        );
        Debug.Log("SAVE "+ stringWordsList.Split('\n').Length);
        PlayerPrefs.SetString(keyName, stringWordsList);
        //wordsListManager.GetSavedGivenWords().ForEach(w => Debug.Log(w));
        screenManager.ShowListTypeMenuScreenPERSO();
    }
}
