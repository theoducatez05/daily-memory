﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameValidationPopup : MonoBehaviour
{

    public PersonalizedListSectionScreenManager screenManager;

    public GameObject errorMassageSection;

    public string keyName { get; set; } //the name of the key inside the playerPref where to find the list of words

    // Start is called before the first frame update
    void Start()
    {
        HideStartGameValidationPopup();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ClickOnPlay()
    {
        if (PlayerPrefs.GetString(keyName).Split('\n').Length == 7 + 1)
        {
            PlayerPrefs.SetInt("listType", GameManager.PERSONALIZED_LIST);
            PlayerPrefs.SetString("PersonalizedWordsList", PlayerPrefs.GetString(keyName));
            SceneManager.LoadScene("ListGameScene");
        }
        else
        {
            StartCoroutine("ShowProblemMessage", "Vous devez avoir une liste de 7 mots pour pouvoir jouer avec cette liste.");
        }
        //Debug.Log("TODO : launch the game with the right list of words");
    }

    public void ClickOnModify()
    {
        screenManager.ShowListModificationScreen(keyName);
        HideStartGameValidationPopup();
    }

    public void ClickOnCancel()
    {
        HideStartGameValidationPopup();
    }

    public void ShowStartGameValidationPopup()
    {
        gameObject.SetActive(true);
    }

    public void HideStartGameValidationPopup()
    {
        gameObject.SetActive(false);
        errorMassageSection.SetActive(false);
    }

    private IEnumerator ShowProblemMessage(string msg)
    {
        errorMassageSection.GetComponentInChildren<TextMeshProUGUI>().text = msg;
        errorMassageSection.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1.0f, 0f, 0f);
        errorMassageSection.SetActive(true);
        yield return new WaitForSeconds(3);
        errorMassageSection.SetActive(false);
    }
}
