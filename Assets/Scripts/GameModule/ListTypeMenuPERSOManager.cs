﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ListTypeMenuPERSOManager : MonoBehaviour
{
    /*private string FIRST_LIST = "firstList";

    private string SECOND_LIST = "secondList";

    private string THIRD_LIST = "thirdList";

    private string FOURTH_LIST = "fourthList";*/

    private List<string> keyNameList;

    public PersonalizedListSectionScreenManager screenManager;

    public List<GameObject> buttonList;

    public StartGameValidationPopup validationPopup;

    // Start is called before the first frame update
    void Awake()
    {
        keyNameList = new List<string>();
        for (int i = 0; i<4; i++)
        {
            keyNameList.Add(i + "List");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        for (int i = 0; i< buttonList.Count; i++)
        {
            buttonList[i].GetComponentInChildren<TextMeshProUGUI>().text = PlayerPrefs.GetString(keyNameList[i] + "Title", "Liste "+ i);
            Debug.Log("PERSO : keyname :" + keyNameList[i]);
        }
    }

    private void HandleListButtonClick(string keyName)
    {
        if (!PlayerPrefs.HasKey(keyName))
        {
            screenManager.ShowListModificationScreen(keyName);
        }
        else
        {
            Debug.Log("TODO : POPUP DE CHOIX DE MODIFICATION OU CHARGEMENT DE LA LSITE EN JEU (OU ANNULATION)");
            validationPopup.keyName = keyName;
            validationPopup.ShowStartGameValidationPopup();
        }
    }

    public void ClickFirstListButton()
    {
        HandleListButtonClick(keyNameList[0]);
    }

    public void ClickSecondListButton()
    {
        HandleListButtonClick(keyNameList[1]);
    }
    public void ClickThirdListButton()
    {
        HandleListButtonClick(keyNameList[2]);
    }
    public void ClickFourthListButton()
    {
        HandleListButtonClick(keyNameList[3]);
    }

    public void ClickOnBack()
    {
        screenManager.GoBackToMainMenu();
    }
}
