﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainGameMenuManager : MonoBehaviour
{

    public GameMenuSceneScreenManager screenManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnAutomaticList()
    {
        screenManager.ShowListTypeMenuScreen();
    }

    public void ClickOnManualList()
    {
        screenManager.ShowPersonalizedListSection();
    }

    public void ClickOnBack()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
