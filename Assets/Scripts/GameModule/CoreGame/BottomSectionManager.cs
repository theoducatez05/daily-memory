﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BottomSectionManager : MonoBehaviour
{
    public GameObject timerUI;
    public GameObject fillInButton;
    public GameObject validateButton;

    private float elapsedTime;
    private bool isTimerLaunched;

    private void Awake()
    {
        ShowFillInButton();
        DeactivateFillInButton();
        this.elapsedTime = 0;
        this.isTimerLaunched = false;
    }

    private void Update()
    {
        if (isTimerLaunched)
        {
            elapsedTime += Time.deltaTime;
            this.timerUI.GetComponentInChildren<TextMeshProUGUI>().text = CreateDrawableTime(Mathf.FloorToInt(this.elapsedTime)) ;
        }
    }

    private void HideAllButtons()
    {
        fillInButton.SetActive(false);
        validateButton.SetActive(false);
    }

    public void DeactivateFillInButton()
    {
        fillInButton.GetComponent<Button>().interactable = false;
        fillInButton.GetComponentInChildren<TextMeshProUGUI>().color = new Color(0.9245283f, 0.9245283f, 0.9245283f);
    }

    public void ActivateFillInButton()
    {
        fillInButton.GetComponent<Button>().interactable = true;
        fillInButton.GetComponentInChildren<TextMeshProUGUI>().color = new Color(0.4392157f, 0.4392157f, 0.4392157f);
    }

    public void ShowFillInButton()
    {
        HideAllButtons();
        fillInButton.SetActive(true);
    }

    public void ShowValidateButton()
    {
        HideAllButtons();
        validateButton.SetActive(true);
    }

    public void LaunchTimer()
    {
        this.isTimerLaunched = true;
    }

    public void StopTimer()
    {
        this.isTimerLaunched = false;
    }

    public int GetElapsedTime()
    {
        return Mathf.FloorToInt(this.elapsedTime);
    }

    private string CreateDrawableTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        string strMinutes = minutes >= 10 ? minutes.ToString() : "0" + minutes.ToString();
        string strSeconds = seconds >= 10 ? seconds.ToString() : "0" + seconds.ToString();

        return strMinutes + ":" + strSeconds;
    }
}
