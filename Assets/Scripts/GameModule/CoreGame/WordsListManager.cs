﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WordsListManager : MonoBehaviour
{
    private GameObject prefabWordItem;
    private GameObject prefabFillInWordItem;

    /// <summary>
    /// List of all gameObject representing words (or fillInWords) on the screen 
    /// </summary>
    private List<GameObject> wordsGameObjectsList;

    /// <summary>
    /// the words given by the user
    /// (the int id is to be sure a word container will save its word in only one specific place)
    /// </summary>
    private Dictionary<int, string> savedGivenWords;

    private void Awake()
    {
        gameObject.SetActive(false);
        this.prefabWordItem = (GameObject)Resources.Load("WordItem");
        this.prefabFillInWordItem = (GameObject)Resources.Load("FillInWordItem");
        this.wordsGameObjectsList = new List<GameObject>();
        this.savedGivenWords = new Dictionary<int, string>();
    }

    public void HideList()
    {
        this.gameObject.SetActive(false);
    }

    public void ShowWordsList(List<string> wordsToShow)
    {
        //Destroy all wordsGameObject (or fillInWordsGameObject) in the list
        if (this.wordsGameObjectsList.Count != 0)
        {
            foreach (GameObject obj in this.wordsGameObjectsList)
            {
                Destroy(obj);
            }
            savedGivenWords = new Dictionary<int, string>();
        }

        this.gameObject.SetActive(true);
        GameObject newObj;
        foreach (string word in wordsToShow)
        {
            // Create new instances of our prefab until we've created as many as we specified
            newObj = (GameObject)Instantiate(prefabWordItem, this.transform);

            //Add this new GameObject in the list to delete it later if needed
            this.wordsGameObjectsList.Add(newObj);

            // Set the word on this object
            newObj.GetComponentInChildren<TextMeshProUGUI>().text = word;
        }
    }

    public void ShowWordsList(List<string> wordsToShow, List<Color> cellsColors)
    {
        //Destroy all wordsGameObject (or fillInWordsGameObject) in the list
        if (this.wordsGameObjectsList.Count != 0)
        {
            foreach (GameObject obj in this.wordsGameObjectsList)
            {
                Destroy(obj);
            }
            savedGivenWords = new Dictionary<int, string>();
        }

        this.gameObject.SetActive(true);
        GameObject newObj;
        int i = 0;
        foreach (string word in wordsToShow)
        {
            // Create new instances of our prefab until we've created as many as we specified
            newObj = (GameObject)Instantiate(prefabWordItem, this.transform);

            //Add this new GameObject in the list to delete it later if needed
            this.wordsGameObjectsList.Add(newObj);

            // Set the word on this object
            newObj.GetComponentInChildren<TextMeshProUGUI>().text = word;

            newObj.GetComponent<Image>().color = cellsColors[i];

            i++;
        }
    }

    public void ShowFillInWordsList(int length)
    {

        //Destroy all wordsGameObject (or fillInWordsGameObject) in the list
        if (this.wordsGameObjectsList.Count != 0)
        {
            foreach (GameObject obj in this.wordsGameObjectsList)
            {
                Destroy(obj);
            }
            savedGivenWords = new Dictionary<int, string>();
        }

        this.gameObject.SetActive(true);
        GameObject newObj;
        for (int i = 0; i<length; i++)
        {
            // Create new instances of our prefab until we've created as many as we specified
            newObj = (GameObject)Instantiate(prefabFillInWordItem, this.transform);

            //Add this new GameObject in the list to delete it later if needed
            this.wordsGameObjectsList.Add(newObj);

            //link the word container with the AddWordToDictionary method to save the word in a Dictionary
            int j = i;
            newObj.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener((word) => AddWordToDictionary(word, j));

            //Initialisation of the dictionary to empty string in order to have all the words
            //(even those who was not fill in) in the list at the end (easier for comparison)
            this.savedGivenWords.Add(j, "");

        }
    }


    public void ShowFillInWordsList(int length, List<string> wordsList)
    {

        //Destroy all wordsGameObject (or fillInWordsGameObject) in the list
        if (this.wordsGameObjectsList.Count != 0)
        {
            foreach (GameObject obj in this.wordsGameObjectsList)
            {
                Destroy(obj);
            }
            savedGivenWords = new Dictionary<int, string>();
        }

        this.gameObject.SetActive(true);
        GameObject newObj;
        for (int i = 0; i < length; i++)
        {
            // Create new instances of our prefab until we've created as many as we specified
            newObj = (GameObject)Instantiate(prefabFillInWordItem, this.transform);

            //Add this new GameObject in the list to delete it later if needed
            this.wordsGameObjectsList.Add(newObj);

            //link the word container with the AddWordToDictionary method to save the word in a Dictionary
            int j = i;
            newObj.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener((word) => AddWordToDictionary(word, j));

            try
            {
                newObj.GetComponentInChildren<TMP_InputField>().text = wordsList[j];
                //Initialisation of the dictionary to word in the list in order to have all the words
                this.savedGivenWords.Add(j, wordsList[j]);
            }
            catch (ArgumentOutOfRangeException)
            {
                //Initialisation of the dictionary to word in the list in order to have all the words
                this.savedGivenWords.Add(j, "");
            }

        }
    }

    public void AddWordToDictionary(string word, int wordContainerId)
    {
        this.savedGivenWords[wordContainerId] = word;
    }

    public List<string> GetSavedGivenWords()
    {
        return new List<string>(this.savedGivenWords.Values);
    }
}
