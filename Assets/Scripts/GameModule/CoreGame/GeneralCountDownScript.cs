﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCountDownScript : MonoBehaviour
{
    public GameManager gameManager;

    public void CountDownAnimationEnded()
    {
        gameManager.LaunchGame();
    }

    public void CountDownFillInAnimationEnded()
    {
        gameManager.LaunchFillInPhase();
    }
}
