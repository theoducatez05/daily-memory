﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SummaryPageManager : MonoBehaviour
{
    public GameManager gameManager;

    public WordsListManager correctWordsListManager;
    public WordsListManager givenWordsListManager;

    public TextMeshProUGUI scoreDisplay;
    public TextMeshProUGUI timeDisplay;

    // Start is called before the first frame update
    void Awake()
    {
        this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        //Set the words in listS
        correctWordsListManager.ShowWordsList(gameManager.displayedWords);
        givenWordsListManager.ShowWordsList(gameManager.givenWords, gameManager.cellsColors);
        //Show the score and the time on the screen
        scoreDisplay.text = gameManager.userScore.ToString();
        timeDisplay.text = CreateDrawableTime(gameManager.elapsedTime);
    }

    public void ShowSummaryPage()
    {
        this.gameObject.SetActive(true);
    }

    private string CreateDrawableTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        string strMinutes = minutes >= 10 ? minutes.ToString() : "0" + minutes.ToString();
        string strSeconds = seconds >= 10 ? seconds.ToString() : "0" + seconds.ToString();

        return strMinutes + ":" + strSeconds;
    }
}
