﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int RANDOM_LIST = 1;

    public static int SHOPPING_LIST = 2;

    public static int PERSONALIZED_LIST = 3;

    public Animator countDown;

    public Animator countDownFillIn;

    public GameObject preGameInfo;

    public BottomSectionManager bottomSectionManager;

    public WordsListManager wordsListManager;

    public SummaryPageManager summaryPageManager;

    public PopupManager cancelPopupManager;

    public PopupManager newBadgePopupManager;

    /// <summary>
    /// All the words available to create a random game
    /// </summary>
    private List<string> words;

    /// <summary>
    /// The list of words display on the screen
    /// </summary>
    public List<string> displayedWords { get; set; }

    /// <summary>
    /// The list of words given by the user
    /// </summary>
    public List<string> givenWords { get; set; }

    /// <summary>
    /// The elapsed time saved when the user clicked on the "RESTITUER" button
    /// </summary>
    public int elapsedTime { get; set; }

    /// <summary>
    /// The calculated score of the user
    /// </summary>
    public int userScore { get; set; }

    /// <summary>
    /// save the color the summary screens cells have to have
    /// </summary>
    public List<Color> cellsColors { get; set; }

    private void Awake()
    {
        this.words = new List<string>();
        this.displayedWords = new List<string>();
        this.givenWords = new List<string>();
        this.elapsedTime = 0;
        this.userScore = 0;
        cellsColors = new List<Color>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if(!PlayerPrefs.HasKey("hasAlreadyPlayed"))
        {
            SavePredefinedWordsList();
        }
        LoadWordsList(PlayerPrefs.GetInt("listType",RANDOM_LIST));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnBegin()
    {
        //Desactiver "PreGameInfo"
        preGameInfo.SetActive(false);
        //Lancer le compte à rebour
        countDown.SetTrigger("LaunchCountDown");
        //Le jeu est lancé par l'animator
    }

    public void LaunchGame()
    {
        Debug.Log("GAME STARTED");
        //réactiver le bouton "RESTITUER"
        bottomSectionManager.ActivateFillInButton();
        //Insérer la liste de mots dans la section prévu (et l'affiche)
        this.displayedWords = SelectRandomWordsInList(7);
        wordsListManager.ShowWordsList(this.displayedWords);
        //Lancer le timer
        bottomSectionManager.LaunchTimer();
    }

    private List<string> SelectRandomWordsInList(int nbWordsToChoose)
    {
        int maxWordsToChooseLimit = Mathf.Min(nbWordsToChoose, words.Count);
        List<string> chosenWords = new List<string>();
        List<int> randomIndexList = new List<int>();
        int randomIndex;
        int saveIt = 0; //when this number become bigger then 10 times the maxWordsToChooseLimit, the loop is exited by force
        for (int i = 0; i<maxWordsToChooseLimit;)
        {
            randomIndex = Random.Range(0, words.Count - 1);
            if (!randomIndexList.Contains(randomIndex))
            {
                chosenWords.Add(words[randomIndex]);
                randomIndexList.Add(randomIndex);
                i++;
            }
            saveIt++;
            if ( saveIt > maxWordsToChooseLimit * 10)
            {
                Debug.Log("BREEEEEAAAAK!!!!");
                break;
            }
        }
        //Debug.Log("RANDOM SELECTION : " + chosenWords.Count);
        return chosenWords;
    }

    /// <summary>
    /// Called when the user click on "RESTITUER"
    /// </summary>
    public void ClickOnFillIn()
    {
        //Stop the timer
        bottomSectionManager.StopTimer();
        //Save the time in a GamaManager attribut
        this.elapsedTime = bottomSectionManager.GetElapsedTime();
        //Cacher la liste des mots
        wordsListManager.HideList();
        //Affiche la page de chargement de restitution
        countDownFillIn.SetTrigger("LaunchCountDownFillIn");
        //Desactiver le bouton "RESTITUER"
        bottomSectionManager.DeactivateFillInButton();
        //La restitution est lancé par l'animator
    }

    /// <summary>
    /// Called at the end of the Count down animation
    /// </summary>
    public void LaunchFillInPhase()
    {
        //Afficher le bouton "VALIDER"
        bottomSectionManager.ShowValidateButton();
        //Affiche les éléments pour restituer les mots
        wordsListManager.ShowFillInWordsList(7);
    }

    /// <summary>
    /// Called when the user click on "VALIDER"
    /// </summary>
    public void ClickOnValidate()
    {
        Debug.Log("VALIDER");
        //Save the results
        this.givenWords = wordsListManager.GetSavedGivenWords();
        //Process the score and save it
        this.calculateUserScore();
        this.saveUseScore();
        //Show the summary page
        summaryPageManager.ShowSummaryPage();
    }

    public void ClickOnGoBackToGameMenu()
    {
        SceneManager.LoadScene("GameMenuScene");
    }

    public void ClickOnCancel()
    {
        cancelPopupManager.ShowPopup();
    }

    private void LoadWordsList(int listType)
    {
        if (listType == RANDOM_LIST)
        {
            List<string> wordsList = new List<string>(PlayerPrefs.GetString("FrenchWordsList").Split('\n'));
            wordsList.ForEach(word => this.words.Add(word.Trim()));
        }
        else if (listType == SHOPPING_LIST)
        {
            List<string> wordsList = new List<string>(PlayerPrefs.GetString("ShoppingFrenchWordsList").Split('\n'));
            wordsList.ForEach(word => this.words.Add(word.Trim()));
        }
        else if (listType == PERSONALIZED_LIST)
        {
            List<string> wordsList = new List<string>(PlayerPrefs.GetString("PersonalizedWordsList").Split('\n'));
            wordsList.ForEach(word => this.words.Add(word.Trim()));
        }
    }

    private void calculateUserScore()
    {
        int score = 0;
        //all in Lower_case
        List<string> displayedWordsLocal = this.displayedWords.ConvertAll(w => w.ToLower());
        List<string> givenWordsLocal = this.givenWords.ConvertAll(w => w.ToLower());
        //Debug.Log("SCORE CALCUL : " + this.displayedWords[0]);
        List<string>.Enumerator givenWordsLocalEnum = givenWordsLocal.GetEnumerator();
        List<string>.Enumerator displayedWordsLocalEnum = displayedWordsLocal.GetEnumerator();
        for(int i=0; i< givenWordsLocal.Count; i++)
        {
            givenWordsLocalEnum.MoveNext();
            displayedWordsLocalEnum.MoveNext();
            //Debug.Log("SI_" + givenWordsEnum.Current + "_==_" + correctWordsEnum.Current + "_:_" + givenWordsEnum.Current.Equals(correctWordsEnum.Current).ToString());
            if (givenWordsLocalEnum.Current.Equals(displayedWordsLocalEnum.Current))
            {
                score += 10;
                cellsColors.Add(new Color(0.4667773f, 0.7735849f, 0.4451762f));
            }
            else
            {
                cellsColors.Add(new Color(0.772549f, 0.4470588f, 0.4470588f));
            }
        }
        score -= this.elapsedTime/3;
        this.userScore = score < 0 ? 0 : score;
    }

    private void saveUseScore()
    {
        //Use duration and score
        StatsManager.GetStatsManager().AddAndSaveScore(this.userScore);
        if (StatsManager.GetStatsManager().CheckForBadges() > 0)
        {
            newBadgePopupManager.ShowPopup();
        }
        Debug.LogWarning("API call to Theo's API (not yet implemented)");
    }

    /// <summary>
    /// Used only during dev to add words in the game
    /// </summary>
    private void SavePredefinedWordsList()
    {
        //RANDOM LIST
        //Read the text from directly from the test.txt file
        TextAsset asset = (TextAsset)Resources.Load("liste_francais");
        PlayerPrefs.SetString("FrenchWordsList", asset.text);
        PlayerPrefs.Save();

        //SHOPPING LIST
        //Read the text from directly from the test.txt file
        asset = (TextAsset)Resources.Load("liste_français_course");
        PlayerPrefs.SetString("ShoppingFrenchWordsList", asset.text);
        PlayerPrefs.Save();
    }
}
