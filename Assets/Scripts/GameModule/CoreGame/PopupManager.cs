﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        HidePopup();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickOnYes()
    {
        SceneManager.LoadScene("GameMenuScene");
    }

    public void ClickOnNo()
    {
        HidePopup();
    }

    public void ClickOnGoToSee()
    {
        SceneManager.LoadScene("StatScene");
    }

    public void ClickOnOK()
    {
        HidePopup();
    }

    public void ShowPopup()
    {
        gameObject.SetActive(true);
    }

    public void HidePopup()
    {
        gameObject.SetActive(false);
    }
}
