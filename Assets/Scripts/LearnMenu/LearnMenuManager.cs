﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LearnMenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenClosePopup()
    {
        this.gameObject.SetActive(!this.gameObject.activeSelf);
    }

    public void GoToAppMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void GoToPractice()
    {
        SceneManager.LoadScene("GameMenuScene");
    }
}
